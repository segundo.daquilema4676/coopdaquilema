

    <div class="site-blocks-cover overlay" style="background-image: url(https://www.coopdaquilema.com/wp-content/uploads/2020/09/giros1.svg);" data-aos="fade" id="home-section">

      <div class="container">
  <div class="row align-items-center justify-content-center">

    <div class="col-md-10 mt-lg-5 text-center">
      <div class="single-text owl-carousel">
        <div class="slide">
          <h1 class="text-uppercase" data-aos="fade-up">Soluciones Financieras</h1>
          <p class="mb-5 desc"  data-aos="fade-up" data-aos-delay="100">En CoopDaquilema, ofrecemos una amplia gama de soluciones financieras para satisfacer tus necesidades.</p>
          <div data-aos="fade-up" data-aos-delay="100">
          </div>
        </div>

        <div class="slide">
          <h1 class="text-uppercase" data-aos="fade-up">Préstamos y Financiamiento</h1>
          <p class="mb-5 desc"  data-aos="fade-up" data-aos-delay="100">Obtén financiamiento para tus proyectos con nuestras opciones flexibles de préstamos.</p>
        </div>

        <div class="slide">
          <h1 class="text-uppercase" data-aos="fade-up">Cuentas de Ahorro</h1>
          <p class="mb-5 desc"  data-aos="fade-up" data-aos-delay="100">Abre una cuenta de ahorro en CoopDaquilema y empieza a planificar tu futuro financiero.</p>
        </div>

      </div>
    </div>

  </div>
</div>


      <a href="#next" class="mouse smoothscroll">
        <span class="mouse-icon">
          <span class="mouse-wheel"></span>
        </span>
      </a>
    </div>

    <div class="site-section" id="next">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-4 text-center" data-aos="fade-up" data-aos-delay="">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/001-wallet.svg'); ?>" alt="Cooperativa de Ahorro y Crédito" class="img-fluid w-25 mb-4">
        <h3 class="card-title">Ahorro de Dinero</h3>
        <p>En nuestra cooperativa, te ayudamos a ahorrar dinero de manera segura y confiable.</p>
      </div>
      <div class="col-md-4 text-center" data-aos="fade-up" data-aos-delay="100">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/004-cart.svg'); ?>" alt="Cooperativa de Ahorro y Crédito" class="img-fluid w-25 mb-4">
        <h3 class="card-title">Compras en Línea</h3>
        <p>Realiza tus compras en línea de forma rápida y segura con nuestros servicios financieros.</p>
      </div>
      <div class="col-md-4 text-center" data-aos="fade-up" data-aos-delay="200">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/006-credit-card.svg'); ?>" alt="Cooperativa de Ahorro y Crédito" class="img-fluid w-25 mb-4">
        <h3 class="card-title">Tarjetas de Crédito / Débito</h3>
        <p>Obtén nuestras tarjetas de crédito o débito y disfruta de beneficios exclusivos.</p>
      </div>

    </div>

    <div class="row">
      <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
        <figure class="circle-bg">
          <img src="<?php echo base_url('static/img/da2.png'); ?>" alt="Cooperativa de Ahorro y Crédito" class="img-fluid">
        </figure>
      </div>
      <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">
        <div class="mb-4">
          <h3 class="h3 mb-4 text-black">Cálculo de Amortización</h3>
          <p>Nuestros expertos te brindarán información detallada sobre cómo calcular la amortización de tus préstamos.</p>
        </div>

        <div class="mb-4">
          <ul class="list-unstyled ul-check success">
            <li>Asesoramiento financiero personalizado</li>
            <li>Opciones flexibles de préstamos</li>
            <li>Acceso a cuentas de ahorro con altas tasas de interés</li>
          </ul>
        </div>

        <div class="mb-4">
          <form action="#">
            <div class="form-group d-flex align-items-center">
              <input type="text" class="form-control mr-2" placeholder="Ingresa tu correo electrónico">
              <input type="submit" class="btn btn-primary" value="Enviar">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>



    <div class="site-section cta-big-image" id="about-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="<?php echo base_url('static/img/da5.png'); ?>" alt="Cooperativa de Ahorro y Crédito" class="img-fluid">
          </div>
          <div class="col-md-6">
            <img src="<?php echo base_url('static/img/da6.png'); ?>" alt="Cooperativa de Ahorro y Crédito" class="img-fluid">
          </div>
        </div>
        <div class="row mb-5 justify-content-center">
          <div class="col-md-8 text-center">
            <h2 class="section-title mb-3" data-aos="fade-up" data-aos-delay="">Nosotros</h2>
            <p class="lead" data-aos="fade-up" data-aos-delay="100">  eslogan  </p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
            <figure class="circle-bg">
            <img src="static/img/da3.png" alt="Free Website Template by Free-Template.co" class="img-fluid">
            </figure>
          </div>
          <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">

            <h3 class="text-black mb-4">Coop Daquilema </h3>

            <p>La Cooperativa de Ahorro y Crédito “Fernando Daquilema” Ltda., Es una organización jurídica que se encuentra legalmente constituida en el país; </p>

            <p>realiza actividades de intermediación financiera y de responsabilidad social con sus socios; y, previa autorización de la Superintendencia de Economía Popular y Solidaria con socios y/o terceros con sujeción a las regulaciones y a los principios reconocidos en la Ley Orgánica de la Economía Popular y Solidaria y del Sector Financiero Popular y Solidario, a su Reglamento General, a las Resoluciones de la Superintendencia de Economía Popular y Solidaria y del ente regulador.</p>

          </div>
        </div>

      </div>
    </div>




    <section class="site-section" id="gallery-section" data-aos="fade">
      <div class="container">
        <div class="row mb-3">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Gallery</h2>
          </div>
        </div>

        <div class="row justify-content-center mb-5">
          <div id="filters" class="filters text-center button-group col-md-7">
            <button class="btn btn-primary active" data-filter="*">All</button>
            <button class="btn btn-primary" data-filter=".web">Events</button>
            <button class="btn btn-primary" data-filter=".design">Party</button>
            <button class="btn btn-primary" data-filter=".brand">Holidays</button>
          </div>
        </div>

        <div id="posts" class="row no-gutter">
          <div class="item web col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
            <a href="<?php echo base_url('static/img/da1.png'); ?>" class="item-wrap fancybox">
              <span class="icon-search2"></span>
              <img class="img-fluid" src="<?php echo base_url('static/img/da1.png'); ?>">
            </a>
          </div>
          <div class="item web col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
            <a href="<?php echo base_url('static/img/da2.png'); ?>" class="item-wrap fancybox" data-fancybox="gallery2">
              <span class="icon-search2"></span>
              <img class="img-fluid" src="<?php echo base_url('static/img/da2.png'); ?>">
            </a>
          </div>

          <div class="item brand col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
            <a href="<?php echo base_url('static/img/da3.png'); ?>" class="item-wrap fancybox" data-fancybox="gallery2">
              <span class="icon-search2"></span>
              <img class="img-fluid" src="<?php echo base_url('static/img/da3.png'); ?>">
            </a>
          </div>

          <div class="item design col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
            <a href="<?php echo base_url('static/img/da4.png'); ?>" class="item-wrap fancybox" data-fancybox="gallery2">
              <span class="icon-search2"></span>
              <img class="img-fluid" src="<?php echo base_url('static/img/da4.png'); ?>">
            </a>
          </div>

          <div class="item web col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
            <a href="<?php echo base_url('static/img/da5.png'); ?>" class="item-wrap fancybox" data-fancybox="gallery2">
              <span class="icon-search2"></span>
              <img class="img-fluid" src="<?php echo base_url('static/img/da5.png'); ?>">
            </a>
          </div>
        </div>

      </div>

    </section>


    <section class="site-section">
  <div class="container">

    <div class="row mb-5 justify-content-center">
      <div class="col-md-7 text-center">
        <h2 class="section-title mb-3" data-aos="fade-up" data-aos-delay="">Cómo Funciona</h2>
        <p class="lead" data-aos="fade-up" data-aos-delay="100">Aquí puedes encontrar información sobre cómo funciona la cooperativa Daquilema.</p>
      </div>
    </div>

    <div class="row align-items-lg-center">
      <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">

        <div class="owl-carousel slide-one-item-alt">
          <img src="<?php echo base_url('static/img/da1.png'); ?>" alt="Image" class="img-fluid">
          <img src="<?php echo base_url('static/img/da2.png'); ?>" alt="Image" class="img-fluid">
          <img src="<?php echo base_url('static/img/da3.png'); ?>" alt="Image" class="img-fluid">
        </div>
        <div class="custom-direction">
          <a href="#" class="custom-prev"><span><span class="icon-keyboard_backspace"></span></span></a><a href="#" class="custom-next"><span><span class="icon-keyboard_backspace"></span></span></a>
        </div>

      </div>
      <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">

        <div class="owl-carousel slide-one-item-alt-text">
          <div>
            <h2 class="section-title mb-3">01. Aplicaciones en Línea</h2>
            <p>Descubre cómo realizar solicitudes en línea a través de la cooperativa Daquilema.</p>

            <p><a href="#" class="btn btn-primary mr-2 mb-2">Más Información</a></p>
          </div>
          <div>
            <h2 class="section-title mb-3">02. Obtén Aprobación</h2>
            <p>Aprende sobre el proceso para obtener aprobación de tus solicitudes.</p>
            <p><a href="#" class="btn btn-primary mr-2 mb-2">Más Información</a></p>
          </div>
          <div>
            <h2 class="section-title mb-3">03. Entrega de Tarjeta</h2>
            <p>Conoce cómo se realiza la entrega de tarjetas a los miembros de la cooperativa Daquilema.</p>

            <p><a href="#" class="btn btn-primary mr-2 mb-2">Más Información</a></p>
          </div>

        </div>

      </div>
    </div>
  </div>
</section>



<section class="site-section border-bottom bg-light" id="services-section">
<div class="container">
<div class="row mb-5">
  <div class="col-12 text-center" data-aos="fade">
    <h2 class="section-title mb-3">Nuestros Servicios</h2>
  </div>
</div>
<div class="row align-items-stretch">
  <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
    <div class="unit-4">
      <div class="unit-4-icon">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/001-wallet.svg'); ?>" alt="Free Website Template by Free-Template.co" class="img-fluid w-25 mb-4">
      </div>
      <div>
        <h3>Asesoría Empresarial</h3>
        <p>Descubre cómo podemos ayudarte con nuestra asesoría empresarial.</p>
        <p><a href="#">Más Información</a></p>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
    <div class="unit-4">
      <div class="unit-4-icon">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/006-credit-card.svg'); ?>" alt="Free Website Template by Free-Template.co" class="img-fluid w-25 mb-4">
      </div>
      <div>
        <h3>Tarjeta de Crédito</h3>
        <p>Descubre nuestras opciones de tarjeta de crédito y sus beneficios.</p>
        <p><a href="#">Más Información</a></p>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
    <div class="unit-4">
      <div class="unit-4-icon">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/002-rich.svg'); ?>" alt="Free Website Template by Free-Template.co" class="img-fluid w-25 mb-4">
      </div>
      <div>
        <h3>Monitoreo de Ingresos</h3>
        <p>Descubre cómo podemos ayudarte a monitorear tus ingresos de manera efectiva.</p>
        <p><a href="#">Más Información</a></p>
      </div>
    </div>
  </div>


  <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="">
    <div class="unit-4">
      <div class="unit-4-icon">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/003-notes.svg'); ?>" alt="Free Website Template by Free-Template.co" class="img-fluid w-25 mb-4">
      </div>
      <div>
        <h3>Asesoría en Seguros</h3>
        <p>Descubre cómo podemos ayudarte con nuestros servicios de asesoría en seguros.</p>
        <p><a href="#">Más Información</a></p>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
    <div class="unit-4">
      <div class="unit-4-icon">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/004-cart.svg'); ?>" alt="Free Website Template by Free-Template.co" class="img-fluid w-25 mb-4">
      </div>
      <div>
        <h3>Inversión Financiera</h3>
        <p>Descubre cómo podemos ayudarte a hacer inversiones financieras inteligentes.</p>
        <p><a href="#">Más Información</a></p>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
    <div class="unit-4">
      <div class="unit-4-icon">
        <img src="<?php echo base_url('static/img/flaticon-svg/svg/005-megaphone.svg'); ?>" alt="Free Website Template by Free-Template.co" class="img-fluid w-25 mb-4">
      </div>
      <div>
        <h3>Administración Financiera</h3>
        <p>Descubre cómo podemos ayudarte con la administración efectiva de tus finanzas.</p>
        <p><a href="#">Más Información</a></p>
      </div>
    </div>
  </div>

</div>
</div>
</section>
<section class="site-section testimonial-wrap" id="testimonials-section" data-aos="fade">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <h2 class="section-title mb-3">Reseñas de Nuestros Clientes</h2>
      </div>
    </div>
  </div>
  <div class="slide-one-item home-slider owl-carousel">
      <div>
        <div class="testimonial">

          <blockquote class="mb-5">
            <p>&ldquo;¡Estoy muy satisfecho con los servicios de la Cooperativa Daquilema! Han sido de gran ayuda en la gestión de mis finanzas y siempre ofrecen soluciones adaptadas a mis necesidades. ¡Recomendado!&rdquo;</p>
          </blockquote>

          <figure class="mb-4 d-flex align-items-center justify-content-center">
            <div><img src="<?php echo base_url('static/img/person_3.png'); ?>" alt="Image" class="w-50 img-fluid mb-3"></div>
            <p>José Pérez</p>
          </figure>
        </div>
      </div>
      <div>
        <div class="testimonial">

          <blockquote class="mb-5">
            <p>&ldquo;Desde que me uní a la Cooperativa Daquilema, he visto un aumento en mis ahorros y una mejor planificación financiera. El personal es amable y siempre dispuesto a ayudar. ¡Una excelente experiencia en general!&rdquo;</p>
          </blockquote>
          <figure class="mb-4 d-flex align-items-center justify-content-center">
            <div><img src="<?php echo base_url('static/img/person_2.png'); ?>" alt="Image" class="w-50 img-fluid mb-3"></div>
            <p>Ana Rodríguez</p>
          </figure>

        </div>
      </div>

      <div>
        <div class="testimonial">

          <blockquote class="mb-5">
            <p>&ldquo;La Cooperativa Daquilema ha sido fundamental en la compra de mi primer hogar. Su equipo de préstamos me guió en cada paso del proceso y me proporcionó una tasa de interés excelente. ¡Gracias por hacer realidad mi sueño de ser propietario!&rdquo;</p>
          </blockquote>
          <figure class="mb-4 d-flex align-items-center justify-content-center">
            <div><img src="<?php echo base_url('static/img/person_4.png'); ?>" alt="Image" class="w-50 img-fluid mb-3"></div>
            <p>Marta Gómez</p>
          </figure>


        </div>
      </div>

      <div>
        <div class="testimonial">

          <blockquote class="mb-5">
            <p>&ldquo;Me alegra haber elegido la Cooperativa Daquilema para mis necesidades financieras. Siempre brindan un servicio excepcional y me hacen sentir valorado como cliente. ¡Sin duda la mejor cooperativa en la que he estado!&rdquo;</p>
          </blockquote>
          <figure class="mb-4 d-flex align-items-center justify-content-center">
            <div><img src="<?php echo base_url('static/img/person_4.png'); ?>" alt="Image" class="w-50 img-fluid mb-3"></div>
            <p>Luis Martínez</p>
          </figure>

        </div>
      </div>

    </div>
</section>






    <section class="site-section" id="about-section">
      <div class="container">

        <div class="row">
          <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
            <figure class="circle-bg">
            <img src="static/img/hero_1.png" alt="Free Website Template by Free-Template.co" class="img-fluid">
            </figure>
          </div>
          <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">


            <div class="row">



              <div class="col-12 mb-4" data-aos="fade-up" data-aos-delay="">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4 mb-3"><span class="text-primary flaticon-head"></span></div>
                  <div>
                    <h3>Bank Loan</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="mb-0"><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-12 mb-4" data-aos="fade-up" data-aos-delay="100">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4 mb-3"><span class="text-primary flaticon-smartphone"></span></div>
                  <div>
                    <h3>Banking Consulation </h3>
                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    <p class="mb-0"><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
            </div>



          </div>
        </div>


      </div>
    </section>




    <section class="site-section" id="blog-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center" data-aos="fade">
        <h2 class="section-title mb-3">Nuestro Blog</h2>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="">
        <div class="h-entry">
          <a href="single.html">
            <img src="<?php echo base_url('static/img/da1.png'); ?>" alt="Image" class="img-fluid">
          </a>
          <h2 class="font-size-regular"><a href="#">Consejos para ahorrar dinero eficientemente</a></h2>
          <div class="meta mb-4">Cooperativa Daquilema <span class="mx-2">&bullet;</span> Enero 18, 2023<span class="mx-2">&bullet;</span> <a href="#">Noticias</a></div>
          <p>Descubre cómo puedes administrar tus finanzas personales de manera efectiva y lograr tus metas de ahorro.</p>
          <p><a href="#">Seguir leyendo...</a></p>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
        <div class="h-entry">
          <a href="single.html">
            <img src="<?php echo base_url('static/img/da4.png'); ?>" alt="Image" class="img-fluid">
          </a>
          <h2 class="font-size-regular"><a href="#">Cómo invertir tus ahorros de forma inteligente</a></h2>
          <div class="meta mb-4">Cooperativa Daquilema <span class="mx-2">&bullet;</span> Enero 18, 2023<span class="mx-2">&bullet;</span> <a href="#">Noticias</a></div>
          <p>Descubre las mejores estrategias para hacer crecer tu dinero y alcanzar la estabilidad financiera.</p>
          <p><a href="#">Seguir leyendo...</a></p>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
        <div class="h-entry">
          <a href="single.html">
            <img src="<?php echo base_url('static/img/da3.png'); ?>" alt="Image" class="img-fluid">
          </a>
          <h2 class="font-size-regular"><a href="#">Los beneficios de solicitar un préstamo personal</a></h2>
          <div class="meta mb-4">Cooperativa Daquilema <span class="mx-2">&bullet;</span> Enero 18, 2023<span class="mx-2">&bullet;</span> <a href="#">Noticias</a></div>
          <p>Descubre cómo puedes usar un préstamo personal de manera inteligente para cumplir tus objetivos financieros.</p>
          <p><a href="#">Seguir leyendo...</a></p>
        </div>
      </div>
    </div>
  </div>
</section>





    <section class="site-section bg-light" id="contact-section" data-aos="fade">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Contact Us</h2>
          </div>
        </div>
        <div class="row mb-5">



          <div class="col-md-4 text-center">
            <p class="mb-4">
              <span class="icon-room d-block h2 text-primary"></span>
              <span>203 Fake St. Mountain View, San Francisco, California, USA</span>
            </p>
          </div>
          <div class="col-md-4 text-center">
            <p class="mb-4">
              <span class="icon-phone d-block h2 text-primary"></span>
              <a href="#">+1 232 3235 324</a>
            </p>
          </div>
          <div class="col-md-4 text-center">
            <p class="mb-0">
              <span class="icon-mail_outline d-block h2 text-primary"></span>
              <a href="#">coopdaquilema@daquilema.com</a>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 mb-5">



            <form action="#" class="p-5 bg-white">

              <h2 class="h4 text-black mb-5">Contact Form</h2>

              <div class="row form-group">
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                  <input type="text" id="fname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                  <input type="text" id="lname" class="form-control">
                </div>
              </div>

              <div class="row form-group">

                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label>
                  <input type="email" id="email" class="form-control">
                </div>
              </div>

              <div class="row form-group">

                <div class="col-md-12">
                  <label class="text-black" for="subject">Subject</label>
                  <input type="subject" id="subject" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label>
                  <textarea name="message" id="message" cols="30" rows="7" class="form-control" placeholder="Write your notes or questions here..."></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-primary btn-md text-white">
                </div>
              </div>


            </form>
          </div>

        </div>
      </div>
    </section>
