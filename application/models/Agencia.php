<?php
class Agencia extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }
  // Insert new agencies
  function insertar($datos){
    $respuesta=$this->db->insert("agencia",$datos);
    return $respuesta;
  }
  // Data query
  function consultarTodos(){
    $agencias=$this->db->get("agencia");
    if ($agencias->num_rows()>0){
      return $agencias->result();
    } else {
      return false;
    }
  }

  // Delete agency by id
  function eliminar($id){
    $this->db->where("idAgencia",$id);
    return $this->db->delete("agencia");
  }
  // Query a single agency
  function obtenerPorId($id){
    $this->db->where("idAgencia",$id);
    $agencia=$this->db->get("agencia");
    if ($agencia->num_rows()>0) {
      return $agencia->row();
    } else {
      return false;
    }
  }


  // Function to update agencies
  function actualizar($id,$datos){
    $this->db->where("idAgencia",$id);
    return $this->db
                ->update("agencia",$datos);
  }

} // End of the class

?>
