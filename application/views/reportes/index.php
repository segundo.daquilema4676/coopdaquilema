<style>
.filtro-btn {
    background-color: #007bff;
    color: white;
    border: none;
    padding: 10px 20px;
    cursor: pointer;
    border-radius: 5px;
    margin: 0 10px; /* Separación entre botones */
}

.filtro-btn:hover {
    background-color: #0056b3;
}

.btn-container {
    text-align: center;
    margin-top: 20px; /* Espacio entre el mapa y los botones */
}
</style>
<div class="btn-container">
    <button class="filtro-btn" onclick="filtrar('agencias')">
        <i class="fas fa-bank"></i> Mostrar Agencias
    </button>
    <button class="filtro-btn" onclick="filtrar('corresponsales')">
        <i class="fas fa-credit-card"></i> Mostrar Corresponsales
    </button>
    <button class="filtro-btn" onclick="filtrar('cajeros')">
        <i class="fas fa-credit-card"></i> Mostrar Cajeros
    </button>

</div>

<div class="modal-body">
    <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;"></div>
</div>
<?php if (!empty($listadoAgencias)): ?>
<script type="text/javascript">
    var miMapa;
    var marcadoresAgencias = [];
    var marcadoresCorresponsales = [];
    var marcadoresCajeros = [];

    function initMap() {
        var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
        miMapa = new google.maps.Map(document.getElementById('reporteMapa'), {
            center: coordenadaCentral,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });


        <?php foreach ($listadoAgencias as $agencia): ?>
        var coordenadaTemporal = new google.maps.LatLng(<?php echo $agencia->latitud; ?>, <?php echo $agencia->longitud; ?>);
        var marcadorAgencia = new google.maps.Marker({
            icon: '<?php echo base_url('static/img/ico1.png'); ?>', // Ruta de la imagen que deseas utilizar como marcador
            position: coordenadaTemporal,
            map: miMapa,
            title: '<?php echo $agencia->nombre; ?>',
        });


        agregarEventosMarcador(marcadorAgencia);
        marcadoresAgencias.push(marcadorAgencia);
        <?php endforeach; ?>


        <?php foreach ($listadoCorresponsales as $corresponsal): ?>
        var coordenadaTemporal = new google.maps.LatLng(<?php echo $corresponsal->latitud; ?>, <?php echo $corresponsal->longitud; ?>);
        var marcadoresCorresponsal = new google.maps.Marker({
            icon: '<?php echo base_url('static/img/ico3.png'); ?>', // Ruta de la imagen que deseas utilizar como marcador
            position: coordenadaTemporal,
            map: miMapa,
            title: '<?php echo $corresponsal->nombre; ?>',
        });


        agregarEventosMarcador(marcadoresCorresponsal);
        marcadoresCorresponsales.push(marcadoresCorresponsal);
        <?php endforeach; ?>

        <?php foreach ($listadoCajeros as $cajero): ?>
        var coordenadaTemporal = new google.maps.LatLng(<?php echo $cajero->latitud; ?>, <?php echo $cajero->longitud; ?>);
        var marcador = new google.maps.Marker({
            icon: '<?php echo base_url('static/img/ico2.png'); ?>', // Ruta de la imagen que deseas utilizar como marcador

            position: coordenadaTemporal,
            map: miMapa,
            title: '<?php echo $cajero->ubicacion; ?>'
        });
        var infoCorresponsal = `
            <div>
                <p><strong>Ubicacion:</strong> <?php echo $corresponsal->ubicacion; ?></p>
                <p><strong>horario_apertura:</strong> <?php echo $corresponsal->estado; ?></p>

            </div>
        `;
        marcadoresCajeros.push(marcador);
        <?php endforeach; ?>
    }

    function filtrar(tipo) {
        ocultarTodosLosMarcadores();
        switch (tipo) {
            case 'agencias':
                mostrarMarcadores(marcadoresAgencias);
                break;
            case 'corresponsales':
                mostrarMarcadores(marcadoresCorresponsales);
                break;
            case 'cajeros':
                mostrarMarcadores(marcadoresCajeros);
                break;
        }
    }

    function ocultarTodosLosMarcadores() {
        ocultarMarcadores(marcadoresAgencias);
        ocultarMarcadores(marcadoresCorresponsales);
        ocultarMarcadores(marcadoresCajeros);
    }

    function ocultarMarcadores(marcadores) {
        marcadores.forEach(function(marcador) {
            marcador.setMap(null);
        });
    }

    function mostrarMarcadores(marcadores) {
        marcadores.forEach(function(marcador) {
            marcador.setMap(miMapa);
        });
    }


    function agregarEventosMarcador(marcador, infoAgencia) {
        var infowindow = new google.maps.InfoWindow({
            content: infoAgencia
        });

        marcador.addListener('mouseover', function() {
            infowindow.open(miMapa, marcador);
        });

        marcador.addListener('mouseout', function() {
            setTimeout(function() {
                infowindow.close();
            }, 2000);
        });
    }
</script>

<?php else: ?>
<div class="alert alert-danger">
    No se encontraron agencias registradas
</div>
<?php endif; ?>
