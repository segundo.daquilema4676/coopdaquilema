<?php
class Corresponsal extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevo Corresponsal
  function insertar($datos){
    $respuesta = $this->db->insert("corresponsal", $datos);
    return $respuesta;
  }

  // Consultar todos los corresponsalesponsals
  function consultarTodos()
{
    $corresponsales = $this->db->get("corresponsal");
    if ($corresponsales->num_rows() > 0) {
        return $corresponsales->result();
    } else {
        return false;
    }
}

  // Eliminar corresponsalesponsal por id
  function eliminar($id){
    $this->db->where("idCorresponsal", $id);
    return $this->db->delete("corresponsal");
  }

  // Obtener Corresponsal por id
  function obtenerPorId($id){
    $this->db->where("idCorresponsal", $id);
    $query = $this->db->get("corresponsal");

    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }

  // Actualizar Corresponsal
  function actualizar($id, $datos){
    $this->db->where("idCorresponsal", $id);
    return $this->db->update("corresponsal", $datos);
  }
  function obtenerPorIdAgencia($idAgencia)
    {
        $this->db->where("idAgencia", $idAgencia);
        $corresponsales = $this->db->get("corresponsal");
        if ($corresponsales->num_rows() > 0) {
            return $corresponsales->result();
        } else {
            return false;
        }
    }
} // Fin de la clase
?>
