<?php

class Reportes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
        $this->load->model("Corresponsal");
        $this->load->model("Cajero");
        
    }

    public function index()
    {
        $data["listadoAgencias"] = $this->Agencia->consultarTodos();
        $data["listadoCorresponsales"] = $this->Corresponsal->consultarTodos();
        $data["listadoCajeros"] = $this->Cajero->consultarTodos();

        $this->load->view('header');
        $this->load->view('reportes/index', $data);
        $this->load->view('footer');
    }

    public function obtener_opciones()
    {
        $filtro = $this->input->post('filtro');
        switch ($filtro) {
            case 'agencia':
                $opciones = $this->Agencia->obtenerOpciones();
                break;
            case 'corresponsal':
                $opciones = $this->Corresponsal->obtenerOpciones();
                break;
            case 'cajero':
                $opciones = $this->Cajero->obtenerOpciones();
                break;
            default:
                $opciones = [];
                break;
        }

        echo json_encode($opciones);
    }

    public function filtrar()
    {
        $filtro = $this->input->post('filtro');
        $id = $this->input->post('id');
        $resultados = [];

        switch ($filtro) {
            case 'agencia':
                $resultados = $this->Agencia->filtrarPorId($id);
                break;
            case 'corresponsal':
                $resultados = $this->Corresponsal->filtrarPorId($id);
                break;
            case 'cajero':
                $resultados = $this->Cajero->filtrarPorId($id);
                break;
            default:
                // Lógica para otros tipos de filtro
                break;
        }

        echo json_encode($resultados);
    }
}

?>
