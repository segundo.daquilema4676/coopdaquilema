<?php
class Clientes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Cliente");

        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoClientes"] = $this->Cliente->consultarTodos();
        $this->load->view("header");
        $this->load->view("clientes/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idCliente)
    {
        $this->Cliente->eliminar($idCliente);
        $this->session->set_flashdata("confirmacion", "Cliente eliminado correctamente");

        redirect("clientes/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("clientes/nuevo");
        $this->load->view("footer");
    }

    public function guardarCliente()
    {
        $config['upload_path'] = APPPATH.'../uploads/clientes/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png';//tipo de archivos permitidos
        $config['max_size'] = 5*1024;//definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "cliente".time()*rand(100, 10000);//creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio;//asignando el nombre al archivo subido
        $this->load->library('upload', $config);//cargando la libreria UPLOAD
        if ($this->upload->do_upload("foto")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data();//capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = "";//Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosNuevoCliente = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "direccion" => $this->input->post("direccion"),
            "ciudad" => $this->input->post("ciudad"),
            "pais" => $this->input->post("pais"),
            "telefono" => $this->input->post("telefono"),
            "email" => $this->input->post("email"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud"),
            "foto" => $nombre_archivo_subido
        );

        $this->Cliente->insertar($datosNuevoCliente);
        $this->session->set_flashdata("confirmacion", "Cliente guardado exitosamente");
        redirect('clientes/index');
    }

    public function editar($idCliente)
    {
        $data["clienteEditar"] = $this->Cliente->obtenerPorId($idCliente);
        $this->load->view("header");
        $this->load->view("clientes/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCliente()
    {
        $idCliente = $this->input->post("idCliente");
        $config['upload_path'] = APPPATH.'../uploads/clientes/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png';//tipo de archivos permitidos
        $config['max_size'] = 5*1024;//definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "cliente".time()*rand(100,10000);//creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio;//asignando el nombre al archivo subido
        $this->load->library('upload', $config);//cargando la libreria UPLOAD
        if ($this->upload->do_upload("foto")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data();//capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = "";//Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosCliente = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "direccion" => $this->input->post("direccion"),
            "ciudad" => $this->input->post("ciudad"),
            "pais" => $this->input->post("pais"),
            "telefono" => $this->input->post("telefono"),
            "email" => $this->input->post("email"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud")
        );
        // Verificar si se seleccionó un nuevo archivo para cargar
        if (!empty($nombre_archivo_subido)) {
          $datosCliente["foto"] = $nombre_archivo_subido;
        }

        $this->Cliente->actualizar($idCliente, $datosCliente);
        $this->session->set_flashdata("confirmacion", "Cliente actualizado exitosamente");
        redirect('clientes/index');
    }

}
?>
