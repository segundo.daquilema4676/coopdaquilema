<h1>EDITAR CLIENTE</h1>
<form method="post" action="<?php echo site_url('clientes/actualizarCliente'); ?>" enctype="multipart/form-data" id="formulario_cliente">
  <input type="hidden" name="idCliente" id="idCliente" value="<?php echo $clienteEditar->idCliente; ?>">
  <label for=""><b>Nombre:</b></label>
  <input type="text" name="nombre" id="nombre" value="<?php echo $clienteEditar->nombre; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for=""><b>Apellido:</b></label>
  <input type="text" name="apellido" id="apellido" value="<?php echo $clienteEditar->apellido; ?>" placeholder="Ingrese el apellido..." class="form-control" required>
  <br>
  <label for=""><b>Dirección:</b></label>
  <input type="text" name="direccion" id="direccion" value="<?php echo $clienteEditar->direccion; ?>" placeholder="Ingrese la dirección..." class="form-control" required>
  <br>
  <label for=""><b>Ciudad:</b></label>
  <input type="text" name="ciudad" id="ciudad" value="<?php echo $clienteEditar->ciudad; ?>" placeholder="Ingrese la ciudad..." class="form-control" required>
  <br>
  <label for=""><b>País:</b></label>
  <input type="text" name="pais" id="pais" value="<?php echo $clienteEditar->pais; ?>" placeholder="Ingrese el país..." class="form-control" required>
  <br>
  <label for=""><b>Teléfono:</b></label>
  <input type="text" name="telefono" id="telefono" value="<?php echo $clienteEditar->telefono; ?>" placeholder="Ingrese el teléfono..." class="form-control" required>
  <br>
  <label for=""><b>Email:</b></label>
  <input type="email" name="email" id="email" value="<?php echo $clienteEditar->email; ?>" placeholder="Ingrese el correo electrónico..." class="form-control" required>
  <br>
  <label for=""><b>Foto:</b></label>
  <?php if (!empty($clienteEditar->foto)): ?>
    <br>
    <img src="<?php echo base_url('uploads/clientes/' . $clienteEditar->foto); ?>" alt="Foto" style="max-width: 200px;">
  <?php else: ?>
    <p>No se ha adjuntado una imagen.</p>
  <?php endif; ?>
  <br>
  <input type="file" name="foto" id="foto" class="form-control">
  <br>
  <div class="row">
    <div class="col-md-6">
      <br>
      <label for=""><b>Latitud:</b></label>
      <input type="number" name="latitud" id="latitud" value="<?php echo $clienteEditar->latitud; ?>" placeholder="Ingrese la latitud..." class="form-control" readonly>
    </div>
    <div class="col-md-6">
      <br>
      <label for=""><b>Longitud:</b></label>
      <input type="number" name="longitud" id="longitud" value="<?php echo $clienteEditar->longitud; ?>" placeholder="Ingrese la longitud..." class="form-control" readonly>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div id="mapa" style="height: 250px; width:100%; border:1px solid black;"></div>
    </div>
  </div>
  <br>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
      <a href="<?php echo site_url('clientes/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
    </div>
  </div>
</form>
<br>
<br>
<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(<?php echo $clienteEditar->latitud; ?>, <?php echo $clienteEditar->longitud; ?>);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccione la ubicación',
      draggable: true
    });
    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
<script type="text/javascript">
    $("#formulario_cliente").validate({
        rules:{
            "nombre": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },
            "apellido": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },
            "direccion": {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            "ciudad": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true
            },
            "pais": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true
            },
            "telefono": {
                required: true,
                minlength: 7,
                maxlength: 15,
                number: true
            },
            "foto": {
                required: true

          },
          "longitud": {
              required: true
          },
          "latitud": {
              required: true

        },
        "email": {
            required: true,
            email:true
        }
        },
        messages:{
            "nombre": {
                required: "Por favor, ingrese su primer nombre",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "apellido": {
                required: "Por favor, ingrese su primer apellido",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "direccion": {
                required: "Por favor, ingrese la dirección",
                minlength: "La dirección debe tener al menos 5 caracteres",
                maxlength: "La dirección no puede tener más de 100 caracteres"
            },
            "ciudad": {
                required: "Por favor, ingrese la ciudad",
                lettersonly: "Solo letras",
                minlength: "La ciudad debe tener al menos 2 caracteres",
                maxlength: "La ciudad no puede tener más de 50 caracteres"
            },
            "pais": {
                required: "Por favor, ingrese el país",
                lettersonly: "Solo letras",
                minlength: "El país debe tener al menos 2 caracteres",
                maxlength: "El país no puede tener más de 50 caracteres"
            },
            "telefono": {
                required: "Por favor, ingrese el teléfono",
                number:"Solo numeros",
                minlength: "El teléfono debe tener al menos 7 caracteres",
                maxlength: "El teléfono no puede tener más de 15 caracteres"
            },
            "foto": {
                required: "Por favor, seleccione una fotografía"
              },
              "longitud": {
                required: "Por favor, ingrese la longitud"
              },
              "latitud": {
                required: "Por favor, ingrese la latitud"

            },
            "email": {
              required: "Por favor, ingrese el email",
              email:"Ingrese un correo valido "
            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
