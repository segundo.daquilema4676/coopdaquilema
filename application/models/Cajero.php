<?php
class Cajero extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevo cajero
  function insertar($datos){
    $respuesta = $this->db->insert("cajero", $datos);
    return $respuesta;
  }

  // Consultar todos los cajeros

  function consultarTodos()
{
    $cajeros = $this->db->get("cajero");
    if ($cajeros->num_rows() > 0) {
        return $cajeros->result();
    } else {
        return false;
    }
}

  // Eliminar cajero por id
  function eliminar($id){
    $this->db->where("idCajero", $id);
    return $this->db->delete("cajero");
  }

  // Obtener cajero por id
  function obtenerPorId($id){
    $this->db->where("idCajero", $id);
    $query = $this->db->get("cajero");

    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }

  // Actualizar cajero
  function actualizar($id, $datos){
    $this->db->where("idCajero", $id);
    return $this->db->update("cajero", $datos);
  }
  function obtenerPorIdAgencia($idAgencia)
    {
        $this->db->where("idAgencia", $idAgencia);
        $cajeros = $this->db->get("cajero");
        if ($cajeros->num_rows() > 0) {
            return $cajeros->result();
        } else {
            return false;
        }
    }

} // Fin de la clase
?>
