<h1>EDITAR TRANSACCIÓN</h1>
<form method="post" action="<?php echo site_url('transacciones/actualizarTransaccion'); ?>" enctype="multipart/form-data">
  <input type="hidden" name="id" id="id" value="<?php echo $transaccionEditar->id; ?>">
  <label for=""><b>CLIENTE:</b></label>
  <br>
  <select name="idCliente" id="idCliente" class="form-control" required>
      <?php foreach ($clientes as $cliente) : ?>
          <option value="<?php echo $cliente->idCliente; ?>" <?php echo ($cliente->idCliente == $transaccionEditar->idCliente) ? 'selected' : ''; ?>><?php echo $cliente->nombre; ?></option>
      <?php endforeach; ?>
  </select>
</div>

<div class="form-group">
  <label for="tipoOperacion"><b>TIPO DE OPERACIÓN:</b></label>
  <select name="tipoOperacion" id="tipoOperacion" class="form-control" required>
    <option value="Deposito" <?php echo ($transaccionEditar->tipoOperacion == 'Deposito') ? 'selected' : ''; ?>>Depósito</option>
    <option value="Retiro" <?php echo ($transaccionEditar->tipoOperacion == 'Retiro') ? 'selected' : ''; ?>>Retiro</option>
  </select>
</div>

<label for="monto">MONTO:</label>
<select name="monto" id="monto" class="form-control" required>
  <option value="<?php echo $transaccionEditar->monto; ?>"><?php echo $transaccionEditar->monto; ?></option>
  <option value="10">10</option>
  <option value="20">20</option>
  <option value="50">50</option>
  <option value="100">100</option>
  <option value="500">500</option>
  <option value="1000">1000</option>
</select>
</div>

<br>

<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
    <a href="<?php echo site_url('transacciones/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
  </div>
</div>
</form>
