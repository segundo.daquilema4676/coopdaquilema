<?php
class Transacciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Transaccion");
        $this->load->model("Cliente");

        // Deshabilitar errores y advertencias de PHP
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoTransacciones"] = $this->Transaccion->consultarTodos();
        $this->load->view("header");
        $this->load->view("transacciones/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id)
    {
        $this->Transaccion->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Transacción eliminada correctamente");
        redirect("transacciones/index");
    }

    public function nueva()    {
        $data["clientes"] = $this->Cliente->consultarTodos();
        $this->load->view("header");
        $this->load->view("transacciones/nueva", $data);
        $this->load->view("footer");
    }
    public function guardarTransaccion() {
    $datosNuevaTransaccion = array(
        "idCliente" => $this->input->post("idCliente"),
        "tipoOperacion" => $this->input->post("tipoOperacion"),
        "monto" => $this->input->post("monto"),
    );

    // Actualizar el saldo del cliente
    $this->Transaccion->actualizarSaldo($datosNuevaTransaccion["idCliente"], $datosNuevaTransaccion["monto"], $datosNuevaTransaccion["tipoOperacion"]);

    // Verificar si es un retiro y el monto es mayor al total
    if ($datosNuevaTransaccion["tipoOperacion"] === "Retiro") {
        $total_actual = $this->Transaccion->obtenerSaldoActual($datosNuevaTransaccion["idCliente"]);
        if ($datosNuevaTransaccion["monto"] > $total_actual) {
            // Mostrar una notificación de fondos insuficientes
            $this->session->set_flashdata("error", "Fondos insuficientes para realizar el retiro");
            redirect('transacciones/index');
        }
    }

    // Obtener el nuevo total del cliente
    $nuevo_total = $this->Transaccion->obtenerSaldoActual($datosNuevaTransaccion["idCliente"]);

    // Agregar el nuevo total al arreglo de datos
    $datosNuevaTransaccion["total"] = $nuevo_total;

    // Insertar la nueva transacción
    $this->Transaccion->insertar($datosNuevaTransaccion);
    $this->session->set_flashdata("confirmacion", "Transacción guardada exitosamente");
    redirect('transacciones/index');
}





    public function editar($id)
    {
        $data["transaccionEditar"] = $this->Transaccion->obtenerPorId($id);
        $data["clientes"] = $this->Cliente->consultarTodos();
        $this->load->view("header");
        $this->load->view("transacciones/editar", $data);
        $this->load->view("footer");
    }
    public function actualizarTransaccion()
{
    $idTransaccion = $this->input->post("id");
    $datosTransaccion = array(
        "idCliente" => $this->input->post("idCliente"),
        "tipoOperacion" => $this->input->post("tipoOperacion"),
        "monto" => $this->input->post("monto"),
    );

    // Obtener el saldo actual del cliente
    $saldo_actual = $this->Transaccion->obtenerSaldoActual($datosTransaccion["idCliente"]);

    // Calcular el nuevo total según el tipo de operación
    if ($datosTransaccion["tipoOperacion"] === "Deposito") {
        $nuevo_total = $saldo_actual + $datosTransaccion["monto"];
    } else {
        $nuevo_total = $saldo_actual - $datosTransaccion["monto"];
    }

    // Actualizar el total en el arreglo de datos
    $datosTransaccion["total"] = $nuevo_total;

    // Actualizar la transacción
    $this->Transaccion->actualizar($idTransaccion, $datosTransaccion);
    $this->session->set_flashdata("confirmacion", "Transacción actualizada exitosamente");
    redirect('transacciones/index');
}




}
?>
