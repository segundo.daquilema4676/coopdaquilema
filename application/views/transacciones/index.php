transacciones<h1>
  <i class="fa fa-credit-card"></i>
  DEPÓSITOS Y RETIROS
</h1>

<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver mapa
    </button>

    <a href="<?php echo site_url('transacciones/nueva');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR TRANSACCIÓN
    </a>
    <br><br>
  </div>
</div>
<?php if ($listadoTransacciones): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>ID Cliente</th>
      <th>Tipo de Operación</th>
      <th>Monto</th>
      <th>Fecha</th>
      <th>Total</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoTransacciones as $transaccion): ?>
    <tr>
      <td><?php echo $transaccion->id; ?></td>
      <td><?php echo $transaccion->idCliente;?> </td>
      <td><?php echo $transaccion->tipoOperacion; ?></td>
      <td><?php echo $transaccion->monto; ?></td>
      <td><?php echo $transaccion->fecha; ?></td>
      <td><?php echo $transaccion->total; ?></td>
      <td>
        <a href="<?php echo site_url('transacciones/editar/') . $transaccion->id; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>
        <a href="<?php echo site_url('transacciones/borrar/') . $transaccion->id; ?>" class="btn btn-danger" title="Eliminar">
          <i class="fa fa-trash"></i>
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          <i class="fa fa-eye"></i> MAPA DE TRANSACCIONES
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </h5>
      </div>
      <div class="modal-body">
        <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">
          <script type="text/javascript">
            function initMap(){
              var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
              var miMapa = new google.maps.Map(
                document.getElementById('reporteMapa'),
                {
                  center: coordenadaCentral,
                  zoom: 8,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                }
              );
              <?php foreach ($listadoTransacciones as $transaccion): ?>
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $transaccion->latitud; ?>, <?php echo $transaccion->longitud; ?>);
                var marcador = new google.maps.Marker({
                  position: coordenadaTemporal,
                  map: miMapa,
                  title: 'Transacción <?php echo $transaccion->id; ?>'
                });
              <?php endforeach; ?>
            }
          </script>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron transacciones registradas
</div>
<?php endif; ?>
