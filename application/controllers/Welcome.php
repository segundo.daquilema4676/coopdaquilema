<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view('headerInfo');//Cargando Cabecera
		$this->load->view('welcome_message');//Cargando Contenido
		$this->load->view('footerInfo');//Cargando Pie
	}
}
