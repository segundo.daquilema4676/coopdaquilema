<?php
class Agencias extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
        $this->load->model("Corresponsal");

        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function reporte()
    {
      $data["listadoCorresponsales"] = $this->Corresponsal->consultarTodos();
        $data["listadoAgencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("agencias/index", $data);
        $this->load->view("footer");
    }

    public function index()
    {
        $data["listadoAgencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("agencias/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idAgencia)
    {
        $this->Agencia->eliminar($idAgencia);
        $this->session->set_flashdata("confirmacion", "Agencia eliminada correctamente");

        redirect("agencias/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("agencias/nuevo");
        $this->load->view("footer");
    }

    public function guardarAgencia()
    {
        $config['upload_path'] = APPPATH.'../uploads/agencias/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png';//tipo de archivos permitidos
        $config['max_size'] = 5*1024;//definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "agencia".time()*rand(100, 10000);//creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio;//asignando el nombre al archivo subido
        $this->load->library('upload', $config);//cargando la libreria UPLOAD
        if ($this->upload->do_upload("foto")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data();//capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = "";//Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosNuevaAgencia = array(
            "nombre" => $this->input->post("nombre"),
            "direccion" => $this->input->post("direccion"),
            "ciudad" => $this->input->post("ciudad"),
            "pais" => $this->input->post("pais"),
            "telefono" => $this->input->post("telefono"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud"),
            "foto" => $nombre_archivo_subido
        );

        $this->Agencia->insertar($datosNuevaAgencia);
        $this->session->set_flashdata("confirmacion", "Agencia guardada exitosamente");
        enviarEmail("segundo.daquilema2001@gmail.com","CREACION",
              "<h1>SUCURSAL AGREGADA EXITOSAMENTE </h1>".$datosNuevoCajero['nombre']);
        redirect('agencias/index');
    }

    public function editar($idAgencia)
    {
        $data["agenciaEditar"] = $this->Agencia->obtenerPorId($idAgencia);
        $this->load->view("header");
        $this->load->view("agencias/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarAgencia()
    {
        $idAgencia = $this->input->post("idAgencia");
        $config['upload_path'] = APPPATH.'../uploads/agencias/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png';//tipo de archivos permitidos
        $config['max_size'] = 5*1024;//definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "agencia".time()*rand(100,10000);//creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio;//asignando el nombre al archivo subido
        $this->load->library('upload', $config);//cargando la libreria UPLOAD
        if ($this->upload->do_upload("foto")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data();//capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = "";//Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosAgencia = array(
            "nombre" => $this->input->post("nombre"),
            "direccion" => $this->input->post("direccion"),
            "ciudad" => $this->input->post("ciudad"),
            "pais" => $this->input->post("pais"),
            "telefono" => $this->input->post("telefono"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud"),
        );
        // Verificar si se seleccionó un nuevo archivo para cargar
        if (!empty($nombre_archivo_subido)) {
          $datosAgencia["foto"] = $nombre_archivo_subido;
        }

        $this->Agencia->actualizar($idAgencia, $datosAgencia);
        $this->session->set_flashdata("confirmacion", "Agencia actualizada exitosamente");
        redirect('agencias/index');
    }

}
?>
