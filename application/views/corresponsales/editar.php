<div class="container">
  <h1 class="mb-4">Editar Corresponsal</h1>
  <form method="post" action="<?php echo site_url('corresponsales/actualizarCorresponsal'); ?>" enctype="multipart/form-data" id="formulario_corresponsal">
    <input type="hidden" name="idCorresponsal" id="idCorresponsal" value="<?php echo $corresponsalEditar->idCorresponsal; ?>">

    <div class="form-group">
      <label for="agencia">Agencia:</label>
      <select name="idAgencia" id="idAgencia" class="form-control">
        <?php foreach ($agencias as $agencia): ?>
          <option value="<?php echo $agencia->idAgencia; ?>"><?php echo $agencia->nombre; ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label for="nombre"><b>Nombre:</b></label>
      <input type="text" name="nombre" id="nombre" value="<?php echo $corresponsalEditar->nombre; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
    </div>

    <div class="form-group">
      <label for="ubicacion"><b>Ubicación:</b></label>
      <input type="text" name="ubicacion" id="ubicacion" value="<?php echo $corresponsalEditar->ubicacion; ?>" placeholder="Ingrese la ubicación..." class="form-control" required>
    </div>

    <div class="form-group">
      <label for="horario_apertura"><b>Horario apertura:</b></label>
      <input type="time" name="horario_apertura" id="horario_apertura" value="<?php echo $corresponsalEditar->horario_apertura; ?>" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="horario_cierre"><b>Horario cierre:</b></label>
      <input type="time" name="horario_cierre" id="horario_cierre" value="<?php echo $corresponsalEditar->horario_cierre; ?>" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="tipo"><b>Tipo:</b></label>
      <input type="text" name="tipo" id="tipo" value="<?php echo $corresponsalEditar->tipo; ?>" placeholder="Ingrese el tipo..." class="form-control" required>
    </div>


    <div class="form-group">

    <label for=""><b>Foto:</b></label>
    <?php if (!empty($corresponsalEditar->foto)): ?>
      <br>
      <img src="<?php echo base_url('uploads/corresponsales/' . $corresponsalEditar->foto); ?>" alt="Foto" style="max-width: 200px;">
    <?php else: ?>
      <p>No se ha adjuntado una imagen.</p>
    <?php endif; ?>
    <br>
    <input type="file" name="foto" id="foto" class="form-control">
    <br>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="latitud"><b>Latitud:</b></label>
          <input type="number" name="latitud" id="latitud" value="<?php echo $corresponsalEditar->latitud; ?>" placeholder="Ingrese la latitud..." class="form-control" readonly>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="longitud"><b>Longitud:</b></label>
          <input type="number" name="longitud" id="longitud" value="<?php echo $corresponsalEditar->longitud; ?>" placeholder="Ingrese la longitud..." class="form-control" readonly>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <div id="mapa" style="height: 250px; width:100%; border:1px solid black;"></div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button>
        <a href="<?php echo site_url('corresponsales/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">

    $.validator.addMethod("greaterThan", function(value, element, params) {
        var apertura = $(params).val().split(":");
        var cierre = value.split(":");

        // Convertir las horas y minutos a números
        var horaApertura = parseInt(apertura[0], 10);
        var minutoApertura = parseInt(apertura[1], 10);
        var horaCierre = parseInt(cierre[0], 10);
        var minutoCierre = parseInt(cierre[1], 10);

        // Comparar las horas y minutos
        if (horaCierre > horaApertura) {
            return true;
        } else if (horaCierre === horaApertura) {
            return minutoCierre > minutoApertura;
        } else {
            return false;
        }
    }, "El horario de cierre debe ser posterior al horario de apertura");

    $("#formulario_corresponsal").validate({
        rules: {
            "nombre": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true
            },
            "ubicacion": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true
            },
            "tipo": {
                required: true,
                minlength: 5,
                maxlength: 100,
                lettersonly: true
            },
            "horario_apertura": {
                required: true,
                validHorario: true
            },
            "horario_cierre": {
                required: true,
                validHorario: true,
                greaterThan: "#horario_apertura"
            },
            "telefono": {
                required: true,
                minlength: 7,
                maxlength: 15,
                number: true
            },
            "longitud": {
                required: true
            },
            "latitud": {
                required: true
            },
            "idAgencia": {
                required: true
            }
        },
        messages: {
            "nombre": {
                required: "Por favor, ingrese su primer nombre",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "ubicacion": {
                required: "Por favor, ingrese la ubicacion",
                lettersonly: "Solo letras",
                minlength: "La ubicacion debe tener al menos 2 caracteres",
                maxlength: "La ubicacion no puede tener más de 100 caracteres"
            },
            "horario_apertura": {
                required: "Por favor, ingrese el horario de apertura",
                validHorario: "El horario de apertura debe ser una hora válida en formato HH:MM"
            },
            "horario_cierre": {
                required: "Por favor, ingrese el horario de cierre",
                validHorario: "El horario de cierre debe ser una hora válida en formato HH:MM",
                greaterThan: "El horario de cierre debe ser posterior al horario de apertura"
            },
            "tipo": {
                required: "Por favor, ingrese el tipo",
                lettersonly: "Solo letras",
                minlength: "El tipo debe tener al menos 2 caracteres",
                maxlength: "El tipo no puede tener más de 50 caracteres"
            },
            "foto": {
                required: "Por favor, seleccione una fotografía"
            },
            "longitud": {
                required: "Por favor, ingrese la longitud"
            },
            "latitud": {
                required: "Por favor, ingrese la latitud"
            },
            "idAgencia": {
                required: "Por favor, seleccione la agencia"
            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>

<br>
<br>
<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(<?php echo $corresponsalEditar->latitud; ?>, <?php echo $corresponsalEditar->longitud; ?>);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccione la ubicación',
      draggable: true
    });
    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
