<h1>EDITAR CAJERO</h1>
<form method="post" action="<?php echo site_url('cajeros/actualizarCajero'); ?>" enctype="multipart/form-data" id="formulario_cajero">
  <input type="hidden" name="idCajero" id="idCajero" value="<?php echo $cajeroEditar->idCajero; ?>">
  <label for=""><b>AGENCIA:</b></label>
  <br>
  <select name="idAgencia" id="idAgencia" class="form-control" required>
      <?php foreach ($agencias as $agencia) : ?>
          <option value="<?php echo $agencia->idAgencia; ?>" <?php echo ($agencia->idAgencia == $cajeroEditar->idAgencia) ? 'selected' : ''; ?>><?php echo $agencia->nombre; ?></option>
      <?php endforeach; ?>
  </select>
</div>
  <label for=""><b>Ubicacion:</b></label>
  <input type="text" name="ubicacion" id="ubicacion" value="<?php echo $cajeroEditar->ubicacion; ?>" placeholder="Ingrese la ubicacion..." class="form-control" required>
  <br>

  <label for=""><b>Estado:</b></label>
  <input type="text" name="estado" id="estado" value="<?php echo $cajeroEditar->estado; ?>" placeholder="Ingrese el estado..." class="form-control" required>
  <br>

  <label for=""><b>Foto:</b></label>
  <?php if (!empty($cajeroEditar->foto)): ?>
      <br>
      <img src="<?php echo base_url('uploads/agencias/' . $cajeroEditar->foto); ?>" alt="Foto" style="max-width: 200px;">
  <?php else: ?>
      <p>No se ha adjuntado un imagen.</p>
  <?php endif; ?>
  <br>
  <input type="file" name="foto" id="foto" class="form-control">
  <br>


  <div class="row">
    <div class="col-md-6">
      <label for=""><b>Latitud:</b></label>
      <input type="number" name="latitud" id="latitud" value="<?php echo $cajeroEditar->latitud; ?>" placeholder="Ingrese la latitud..." class="form-control" readonly>
    </div>
    <div class="col-md-6">
      <label for=""><b>Longitud:</b></label>
      <input type="number" name="longitud" id="longitud" value="<?php echo $cajeroEditar->longitud; ?>" placeholder="Ingrese la longitud..." class="form-control" readonly>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div id="mapa" style="height: 250px; width:100%; border:1px solid black;"></div>
    </div>
  </div>
  <br>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
      <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
    </div>
  </div>
</form>

<br>
<br>
<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(<?php echo $cajeroEditar->latitud; ?>, <?php echo $cajeroEditar->longitud; ?>);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccione la ubicación',
      draggable: true
    });
    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
<script type="text/javascript">
    $("#formulario_cajero").validate({
        rules:{
            "ubicacion": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },

            "estado": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true

            },
            "foto": {
                required: true

          },
          "longitud": {
              required: true
          },
          "latitud": {
              required: true
          }
        },
        messages:{
            "ubicacion": {
                required: "Por favor, ingrese la ubicacion",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "estado": {
                required: "Por favor, ingrese el estado",
                lettersonly: "Solo letras",
                minlength: "El país debe tener al menos 2 caracteres",
                maxlength: "El país no puede tener más de 50 caracteres"
            },

            "foto": {
                required: "Por favor, seleccione una fotografía"
              },
              "longitud": {
                required: "Por favor, ingrese la longitud"
              },
              "latitud": {
                required: "Por favor, ingrese la latitud"
              }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
