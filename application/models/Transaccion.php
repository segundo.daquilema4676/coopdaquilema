<?php
class Transaccion extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nueva transacción
  function insertar($datos){
    $respuesta = $this->db->insert("depositosRetiros", $datos);
    return $respuesta;
  }

  // Consultar todas las transacciones
  function consultarTodos()
  {
    $transacciones = $this->db->get("depositosRetiros");
    if ($transacciones->num_rows() > 0) {
        return $transacciones->result();
    } else {
        return false;
    }
  }

  // Eliminar transacción por id
  function eliminar($id){
    $this->db->where("id", $id);
    return $this->db->delete("depositosRetiros");
  }

  // Obtener transacción por id
  function obtenerPorId($id){
    $this->db->where("id", $id);
    $query = $this->db->get("depositosRetiros");

    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }

  // Actualizar transacción
  function actualizar($id, $datos){
    $this->db->where("id", $id);
    return $this->db->update("depositosRetiros", $datos);
  }

  // Obtener transacciones por id de cliente
  function obtenerPorIdCliente($idCliente)
  {
    $this->db->where("idCliente", $idCliente);
    $transacciones = $this->db->get("depositosRetiros");
    if ($transacciones->num_rows() > 0) {
        return $transacciones->result();
    } else {
        return false;
    }
  }
  public function obtenerSaldoActual($idCliente)
    {
        $query = $this->db->select('total')
            ->from('depositosRetiros')
            ->where('idCliente', $idCliente)
            ->get();

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->saldo;
        } else {
            return 0;
        }
    }
    function actualizarSaldo($idCliente, $monto, $tipoOperacion) {
    // Establecer el signo de la operación
    $signo = ($tipoOperacion === "Deposito") ? "+" : "-";

    // Construir la consulta SQL para actualizar el saldo
    $sql = "UPDATE depositosRetiros SET total = total $signo ? WHERE idCliente = ?";

    // Ejecutar la consulta SQL con los parámetros proporcionados
    $this->db->query($sql, array($monto, $idCliente));
}
private function calcularNuevoTotal($saldo_actual, $monto, $tipoOperacion) {
    // Inicializar el nuevo total con el saldo actual
    $nuevo_total = $saldo_actual;

    // Verificar el tipo de operación y realizar la suma o resta según corresponda
    if ($tipoOperacion === "Deposito") {
        $nuevo_total += $monto;
    } elseif ($tipoOperacion === "Retiro") {
        // Verificar si el monto del retiro es menor o igual al saldo actual
        if ($monto <= $saldo_actual) {
            $nuevo_total -= $monto;
        } else {
            // Si el monto del retiro es mayor que el saldo actual, lanzar una excepción o manejarlo según tus necesidades
            throw new Exception("El monto del retiro es mayor que el saldo actual");
        }
    }

    // Devolver el nuevo total calculado
    return $nuevo_total;
}



} // Fin de la clase
?>
