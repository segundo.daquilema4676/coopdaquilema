<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO CORRESPONSAL
  </b>
</h1>

<div class="container">
  <div class="row">
    <div class="col-md-6">
      <form action="<?php echo site_url('corresponsales/guardarCorresponsal');?>" method="post" enctype="multipart/form-data" id="formulario_corresponsal">
        <div class="form-group">
          <label for="agencia">Agencia:</label>
          <select name="idAgencia" id="idAgencia" class="form-control">
            <?php foreach ($agencias as $agencia): ?>
              <option value="<?php echo $agencia->idAgencia; ?>"><?php echo $agencia->nombre; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group">
          <label for="nombre">NOMBRE:</label>
          <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre">
        </div>
        <div class="form-group">
          <label for="ubicacion">UBICACION:</label>
          <input type="text" name="ubicacion" id="ubicacion" class="form-control" placeholder="Ingrese la ubicacion">
        </div>
        <div class="form-group">
          <label for="tipo">TIPO:</label>
          <input type="text" name="tipo" id="tipo" class="form-control" placeholder="Ingrese el tipo">
        </div>
        <div class="form-group">
          <label for="horario_apertura">HORARIO APERTURA:</label>
          <input type="time" name="horario_apertura" id="horario_apertura" class="form-control" placeholder="Ingrese el horario de apertura">
        </div>
        <div class="form-group">
          <label for="horario_cierre">HORARIO CIERRE:</label>
          <input type="time" name="horario_cierre" id="horario_cierre" class="form-control" placeholder="Ingrese el horario de cierre">
        </div>
          <div class="form-group">
            <form class="" action="index.html" method="post"> <label for="">LATITUD:</label>
            <input type="number" name="latitud" id="latitud" class="form-control" placeholder="Ingrese el latitud" readonly>
              </div>
          <div class="form-group">
            <form class="" action="index.html" method="post"> <label for="">LONGITUD:</label>
            <input type="number" name="longitud" id="longitud" class="form-control" placeholder="Ingrese la longitud" readonly>
          </div>
        <div class="form-group">
          <label for="foto">Fotografia:</label>
          <input type="file" name="foto" id="foto" class="form-control" required accept="image/*">
        </div>
        <br>
        <div class="form-group">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
          <a href="<?php echo site_url('corresponsales/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i> CANCELAR</a>
        </div>

      </form>
    </div>
    <div class="col-md-6">
      <div id="mapa" style="height: 350px; width: 100%; border: 1px solid black;"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $("#formulario_corresponsal").validate({
        rules:{
            "nombre": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },
            "ubicacion": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },
            "tipo": {
                required: true,
                minlength: 5,
                maxlength: 100,
                lettersonly: true

            },
            "horario_apertura": {
              required: true,
              validHorario: true
          },
          "horario_cierre": {
              required: true,
              validHorario: true,
              greaterThan: "#horario_apertura"

    },
            "telefono": {
                required: true,
                minlength: 7,
                maxlength: 15,
                number: true
            },
            "foto": {
                required: true

          },
          "longitud": {
              required: true
          },
          "latitud": {
              required: true

        },
        "idAgencia":{
          required: true


        }

        },
        messages:{
            "nombre": {
                required: "Por favor, ingrese su primer nombre",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "ubicacion": {
                required: "Por favor, ingrese la ubicacion",
                lettersonly: "Solo letras",
                minlength: "La ubicacion debe tener al menos 2 caracteres",
                maxlength: "La ubicacion no puede tener más de 100 caracteres"
            },
            "horario_apertura": {
            required: "Por favor, ingrese el horario de apertura",
        },
        "horario_cierre": {
            required: "Por favor, ingrese el horario de cierre",
            greaterThan: "El horario de cierre debe ser posterior al horario de apertura"
        },
            "tipo": {
                required: "Por favor, ingrese el tipo",
                lettersonly: "Solo letras",
                minlength: "El tipo debe tener al menos 2 caracteres",
                maxlength: "El tipo no puede tener más de 50 caracteres"
            },

            "foto": {
                required: "Por favor, seleccione una fotografía"
              },
              "longitud": {
                required: "Por favor, ingrese la longitud"
              },
              "latitud": {
                required: "Por favor, ingrese la latitud"

            },
            "idAgencia":{
              required: "Por favor, seleccione la agencia"


            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });

    $.validator.addMethod("validHorario", function(value, element) {
        // Expresión regular para validar el formato HH:MM
        var validFormat = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
        return this.optional(element) || validFormat.test(value);
    }, "Por favor ingrese un horario válido en formato HH:MM");

    $.validator.addMethod("greaterThan", function(value, element, params) {
        var apertura = $(params).val().split(":");
        var cierre = value.split(":");

        // Convertir las horas y minutos a números
        var horaApertura = parseInt(apertura[0], 10);
        var minutoApertura = parseInt(apertura[1], 10);
        var horaCierre = parseInt(cierre[0], 10);
        var minutoCierre = parseInt(cierre[1], 10);

        // Comparar las horas y minutos
        if (horaCierre > horaApertura) {
            return true;
        } else if (horaCierre === horaApertura) {
            return minutoCierre > minutoApertura;
        } else {
            return false;
        }
    }, "El horario de cierre debe ser posterior al horario de apertura");
</script>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-0.18522025692690522, -78.47587495002826);

    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Selecciona la ubicacion',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
