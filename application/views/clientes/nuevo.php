<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO CLIENTE
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('clientes/guardarCliente');?>" method="post" enctype="multipart/form-data" id="formulario_cliente">
  <div class="row">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-6">
          <br>
          <label for="">NOMBRE:</label>
          <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre">
        </div>
        <div class="col-md-6">
          <br>
          <label for="">APELLIDO:</label>
          <input type="text" name="apellido" id="apellido" class="form-control" placeholder="Ingrese el apellido">
        </div>
      </div>

      <br>
      <label for="">DIRECCIÓN:</label>
      <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingrese la dirección">
      <br>

      <label for="">CIUDAD:</label>
      <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="Ingrese la ciudad">
      <br>

      <label for="">PAÍS:</label>
      <input type="text" name="pais" id="pais" class="form-control" placeholder="Ingrese el país">
      <br>

      <label for="">TELÉFONO:</label>
      <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Ingrese el teléfono">
      <br>

      <label for="">EMAIL:</label>
      <input type="email" name="email" id="email" class="form-control" placeholder="Ingrese el correo electrónico">
      <br>

      <div class="row">
        <div class="col-md-6">
          <label for="">LATITUD:</label>
          <input type="number" name="latitud" id="latitud" class="form-control" placeholder="Ingrese la latitud" readonly>
        </div>

        <div class="col-md-6">
          <label for="">LONGITUD:</label>
          <input type="number" name="longitud" id="longitud" class="form-control" placeholder="Ingrese la longitud" readonly>
        </div>
      </div>

      <br>
      <b>Fotografía:</b>
      <input type="file" name="foto" id="foto" class="form-control" required accept="image/*">
    </div>

    <div class="col-md-6">
      <br>
      <div class="row">
        <div class="col-md-12">
          <br>
          <div id="mapa" style="height:350px; width: 100%; border:1px solid black;"></div>
          <br>
          <br>
          <br>
          <div class="row">
            <div class="cold-md-12 text-center">
              <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
              <a href="<?php echo site_url('clientes/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i>
                CANCELAR
              </a>
            </div>
          </div>
        </div>
      </div>
      <br>
    </div>
  </div>
</form>
<br><br>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-0.18522025692690522, -78.47587495002826);

    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Selecciona la ubicacion',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
<script type="text/javascript">
    $("#formulario_cliente").validate({
        rules:{
            "nombre": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },
            "apellido": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },
            "direccion": {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            "ciudad": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true
            },
            "pais": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true
            },
            "telefono": {
                required: true,
                minlength: 7,
                maxlength: 15,
                number: true
            },
            "foto": {
                required: true

          },
          "longitud": {
              required: true
          },
          "latitud": {
              required: true

        },
        "email": {
            required: true,
            email:true
        }
        },
        messages:{
            "nombre": {
                required: "Por favor, ingrese su primer nombre",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "apellido": {
                required: "Por favor, ingrese su primer apellido",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "direccion": {
                required: "Por favor, ingrese la dirección",
                minlength: "La dirección debe tener al menos 5 caracteres",
                maxlength: "La dirección no puede tener más de 100 caracteres"
            },
            "ciudad": {
                required: "Por favor, ingrese la ciudad",
                lettersonly: "Solo letras",
                minlength: "La ciudad debe tener al menos 2 caracteres",
                maxlength: "La ciudad no puede tener más de 50 caracteres"
            },
            "pais": {
                required: "Por favor, ingrese el país",
                lettersonly: "Solo letras",
                minlength: "El país debe tener al menos 2 caracteres",
                maxlength: "El país no puede tener más de 50 caracteres"
            },
            "telefono": {
                required: "Por favor, ingrese el teléfono",
                number:"Solo numeros",
                minlength: "El teléfono debe tener al menos 7 caracteres",
                maxlength: "El teléfono no puede tener más de 15 caracteres"
            },
            "foto": {
                required: "Por favor, seleccione una fotografía"
              },
              "longitud": {
                required: "Por favor, ingrese la longitud"
              },
              "latitud": {
                required: "Por favor, ingrese la latitud"

            },
            "email": {
              required: "Por favor, ingrese el email",
              email:"Ingrese un correo valido "
            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
