<?php
class Corresponsales extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Corresponsal");
        $this->load->model("Agencia");


        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoCorresponsales"] = $this->Corresponsal->consultarTodos();
        $datos["listadoAgencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("corresponsales/index", $data);
        $this->load->view("footer");
    }


    public function borrar($idCorresponsal)
{
    $this->Corresponsal->eliminar($idCorresponsal);
    $this->session->set_flashdata("confirmacion", "Corresponsal eliminado correctamente");

    redirect("corresponsales/index");
}

public function nuevo()
{
  $data["agencias"] = $this->Agencia->consultarTodos();
  $this->load->view("header");
  $this->load->view("corresponsales/nuevo", $data);
  $this->load->view("footer");
}
    public function guardarCorresponsal(){

      /* INICIO PROCESO DE SUBIDA DE ARCHIVO */
      $config['upload_path']=APPPATH.'../uploads/corresponsales/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio = "corresponsal_" . uniqid(); // Generar un nombre de archivo único
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("foto")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }

        $datosNuevoCorresponsal = array(
          "idAgencia" => $this->input->post("idAgencia"),
           "nombre" => $this->input->post("nombre"),
            "ubicacion" => $this->input->post("ubicacion"),
            "tipo" => $this->input->post("tipo"),
            "horario_apertura" => $this->input->post("horario_apertura"),
            "horario_cierre" => $this->input->post("horario_cierre"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud"),
            "foto" => $nombre_archivo_subido
        );

        $this->Corresponsal->insertar($datosNuevoCorresponsal);
        $this->session->set_flashdata("confirmacion", "Corresponsal guardado exitosamente");
+        redirect('corresponsales/index');
    }

    public function editar($idCorresponsal)
    {
        $data["corresponsalEditar"] = $this->Corresponsal->obtenerPorId($idCorresponsal);
        $data["agencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("corresponsales/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCorresponsal()
  {
      $idCorresponsal = $this->input->post("idCorresponsal");
      $config['upload_path']=APPPATH.'../uploads/corresponsales/'; //ruta de subida de archivos
        $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
        $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
        $nombre_aleatorio="corresponsal".time()*rand(100,10000);//creando un nombre aleatorio
        $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
        $this->load->library('upload',$config);//cargando la libreria UPLOAD
        if($this->upload->do_upload("foto")){ //intentando subir el archivo
            $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
            $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        }else{
            $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
        }

      $datosCorresponsal = array(
          "idAgencia" => $this->input->post("idAgencia"), // Nueva línea para obtener idAgencia desde el formulario
          "nombre" => $this->input->post("nombre"),
           "ubicacion" => $this->input->post("ubicacion"),
           "tipo" => $this->input->post("tipo"),
           "horario_apertura" => $this->input->post("horario_apertura"),
           "horario_cierre" => $this->input->post("horario_cierre"),
           "latitud" => $this->input->post("latitud"),
           "longitud" => $this->input->post("longitud"),
      );

      // Verificar si se seleccionó un nuevo archivo para cargar
      if (!empty($nombre_archivo_subido)) {
          $datosCorresponsal["foto"] = $nombre_archivo_subido;
      }

      $this->Corresponsal->actualizar($idCorresponsal, $datosCorresponsal);
      $this->session->set_flashdata("confirmacion", "Corresponsal actualizado exitosamente");
      redirect('corresponsales/index');
  }

}
?>
