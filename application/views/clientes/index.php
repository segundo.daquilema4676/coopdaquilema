<h1>
  <i class="fa fa-bank"></i>
  CLIENTES
</h1>
<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver mapa
    </button>

    <a href="<?php echo site_url('clientes/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR CLIENTE
    </a>
    <br><br>
  </div>
</div>
<?php if ($listadoClientes): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>NOMBRE</th>
      <th>APELLIDO</th>
      <th>DIRECCIÓN</th>
      <th>CIUDAD</th>
      <th>PAÍS</th>
      <th>TELÉFONO</th>
      <th>EMAIL</th>
      <th>LATITUD</th>
      <th>LONGITUD</th>
      <th>FOTO</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoClientes as $cliente): ?>
    <tr>
      <td><?php echo $cliente->idCliente; ?></td>
      <td><?php echo $cliente->nombre; ?></td>
      <td><?php echo $cliente->apellido; ?></td>
      <td><?php echo $cliente->direccion; ?></td>
      <td><?php echo $cliente->ciudad; ?></td>
      <td><?php echo $cliente->pais; ?></td>
      <td><?php echo $cliente->telefono; ?></td>
      <td><?php echo $cliente->email; ?></td>
      <td><?php echo $cliente->latitud; ?></td>
      <td><?php echo $cliente->longitud; ?></td>
      <td>
          <?php if ($cliente->foto!=""): ?>
            <img src="<?php echo base_url('uploads/clientes/').$cliente->foto; ?>" height="100px" alt="">
          <?php else: ?>
            N/A
          <?php endif; ?>
        </td>

      <td>
        <a href="<?php echo site_url('clientes/editar/').$cliente->idCliente; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>

        <a href="<?php echo site_url('clientes/borrar/').$cliente->idCliente; ?>">Eliminar</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          <i class="fa fa-eye"></i> MAPA DE CLIENTES
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </h5>
      </div>
      <div class="modal-body">
        <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">
          <script type="text/javascript">
            function initMap(){
                var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
                var miMapa = new google.maps.Map(
                    document.getElementById('reporteMapa'),
                    {
                        center: coordenadaCentral,
                        zoom: 8,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                );
                <?php foreach ($listadoClientes as $cliente): ?>
                    var coordenadaTemporal = new google.maps.LatLng(<?php echo $cliente->latitud; ?>, <?php echo $cliente->longitud; ?>);
                    var icono = {
                        url: '<?php echo base_url('static/img/ico1.png'); ?>', // Ruta de la imagen que deseas utilizar como marcador
                        scaledSize: new google.maps.Size(50, 50), // Tamaño de la imagen
                        origin: new google.maps.Point(0, 0), // Origen de la imagen
                        anchor: new google.maps.Point(25, 50) // Punto de anclaje de la imagen
                    };
                    var marcador = new google.maps.Marker({
                        position: coordenadaTemporal,
                        map: miMapa,
                        title: '<?php echo $cliente->nombre; ?>',
                        icon: icono // Establece el icono personalizado
                    });
                <?php endforeach; ?>
            }
        </script>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron clientes registrados
</div>
<?php endif; ?>
