<?php
class Cajeros extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Cajero");
        $this->load->model("Agencia");

        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoCajeros"] = $this->Cajero->consultarTodos();
        $datos["listadoAgencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("cajeros/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idCajero)
    {
        $this->Cajero->eliminar($idCajero);
        $this->session->set_flashdata("confirmacion", "Cajero eliminado correctamente");
        redirect("cajeros/index");
    }

    public function nuevo()
    {
        $data["agencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("cajeros/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarCajero()
    {
        /* INICIO PROCESO DE SUBIDA DE ARCHIVO */
        $config['upload_path'] = APPPATH.'../uploads/cajeros/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png';//tipo de archivos permitidos
        $config['max_size'] = 5*1024;//definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "cajero_" . uniqid(); // Generar un nombre de archivo único
        $config['file_name'] = $nombre_aleatorio;//asignando el nombre al archivo subido
        $this->load->library('upload', $config);//cargando la libreria UPLOAD
        if($this->upload->do_upload("foto")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data();//capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = "";//Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosNuevoCajero = array(
            "idAgencia" => $this->input->post("idAgencia"),
            "ubicacion" => $this->input->post("ubicacion"),
            "estado" => $this->input->post("estado"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud"),
            "foto" => $nombre_archivo_subido
        );

        $this->Cajero->insertar($datosNuevoCajero);
        $this->session->set_flashdata("confirmacion", "Cajero guardado exitosamente");
        redirect('cajeros/index');
    }

    public function editar($idCajero)
    {
        $data["cajeroEditar"] = $this->Cajero->obtenerPorId($idCajero);
        $data["agencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("cajeros/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCajero()
    {
        $idCajero = $this->input->post("idCajero");
        $config['upload_path'] = APPPATH.'../uploads/cajeros/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png';//tipo de archivos permitidos
        $config['max_size'] = 5*1024;//definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "cajero".time()*rand(100,10000);//creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio;//asignando el nombre al archivo subido
        $this->load->library('upload', $config);//cargando la libreria UPLOAD
        if ($this->upload->do_upload("foto")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data();//capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = "";//Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosCajero = array(
            "idAgencia" => $this->input->post("idAgencia"),
            "ubicacion" => $this->input->post("ubicacion"),
            "estado" => $this->input->post("estado"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud"),
        );

        // Verificar si se seleccionó un nuevo archivo para cargar
        if (!empty($nombre_archivo_subido)) {
            $datosCajero["foto"] = $nombre_archivo_subido;
        }

        $this->Cajero->actualizar($idCajero, $datosCajero);
        $this->session->set_flashdata("confirmacion", "Cajero actualizado exitosamente");
        redirect('cajeros/index');
    }

}
?>
