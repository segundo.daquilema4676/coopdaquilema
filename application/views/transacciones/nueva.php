<div class="container">
  <h1>
    <b>
      <i class="fa fa-plus-circle"></i>
      NUEVA TRANSACCIÓN
    </b>
  </h1>
  <br>
  <div class="row">
    <div class="col-md-6">
      <form action="<?php echo site_url('transacciones/guardarTransaccion');?>" method="post" enctype="multipart/form-data">

        <div class="form-group">
          <label for=""><b>CLIENTE:</b></label>
          <br>
          <select name="idCliente" id="idCliente" class="form-control" required>
              <?php foreach ($clientes as $cliente) : ?>
                  <option value="<?php echo $cliente->idCliente; ?>"><?php echo $cliente->nombre; ?></option>
              <?php endforeach; ?>
          </select>
        </div>

        <div class="form-group">
          <label for="tipoOperacion">TIPO DE OPERACIÓN:</label>
          <select name="tipoOperacion" id="tipoOperacion" class="form-control" required>
            <option value="Deposito">Depósito</option>
            <option value="Retiro">Retiro</option>
          </select>
        </div>

        <div class="form-group">
  <label for="monto">MONTO:</label>
  <select name="monto" id="monto" class="form-control" required>
    <option value="">Selecciona un monto</option>
    <option value="10">10</option>
    <option value="20">20</option>
    <option value="50">50</option>
    <option value="100">100</option>
    <option value="500">500</option>
    <option value="1000">1000</option>
  </select>
</div>

        <br>
        <div class="form-group">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
          <a href="<?php echo site_url('transacciones/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i> CANCELAR</a>
        </div>
      </form>
    </div>
  </div>
</div>
