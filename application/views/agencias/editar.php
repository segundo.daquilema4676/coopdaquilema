<h1>EDITAR agencia</h1>
<form method="post" action="<?php echo site_url('agencias/actualizarAgencia'); ?>" enctype="multipart/form-data" id="formulario_agencia">
  <input type="hidden" name="idAgencia" id="idAgencia" value="<?php echo $agenciaEditar->idAgencia; ?>">
  <label for=""><b>Nombre:</b></label>
  <input type="text" name="nombre" id="nombre" value="<?php echo $agenciaEditar->nombre; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for=""><b>Dirección:</b></label>
  <input type="text" name="direccion" id="direccion" value="<?php echo $agenciaEditar->direccion; ?>" placeholder="Ingrese la dirección..." class="form-control" required>
  <br>
  <label for=""><b>Ciudad:</b></label>
  <input type="text" name="ciudad" id="ciudad" value="<?php echo $agenciaEditar->ciudad; ?>" placeholder="Ingrese la ciudad..." class="form-control" required>
  <br>
  <label for=""><b>País:</b></label>
  <input type="text" name="pais" id="pais" value="<?php echo $agenciaEditar->pais; ?>" placeholder="Ingrese el país..." class="form-control" required>
  <br>
  <label for=""><b>Teléfono:</b></label>
  <input type="text" name="telefono" id="telefono" value="<?php echo $agenciaEditar->telefono; ?>" placeholder="Ingrese el teléfono..." class="form-control" required>
  <br>
    <label for=""><b>Foto:</b></label>
    <?php if (!empty($agenciaEditar->foto)): ?>
        <br>
        <img src="<?php echo base_url('uploads/agencias/' . $agenciaEditar->foto); ?>" alt="Foto" style="max-width: 200px;">
    <?php else: ?>
        <p>No se ha adjuntado un imagen.</p>
    <?php endif; ?>
    <br>
    <input type="file" name="foto" id="foto" class="form-control">
    <br>


  <div class="row">
    <div class="col-md-6">
      <br>
      <label for=""><b>Latitud:</b></label>
      <input type="number" name="latitud" id="latitud" value="<?php echo $agenciaEditar->latitud; ?>" placeholder="Ingrese la latitud..." class="form-control" readonly>
    </div>
    <div class="col-md-6">
      <br>
      <label for=""><b>Longitud:</b></label>
      <input type="number" name="longitud" id="longitud" value="<?php echo $agenciaEditar->longitud; ?>" placeholder="Ingrese la longitud..." class="form-control" readonly>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div id="mapa" style="height: 250px; width:100%; border:1px solid black;"></div>
    </div>
  </div>
  <br>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
      <a href="<?php echo site_url('agenciaes/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
    </div>
  </div>
</form>
<br>
<br>
<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(<?php echo $agenciaEditar->latitud; ?>, <?php echo $agenciaEditar->longitud; ?>);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccione la ubicación',
      draggable: true
    });
    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
<script type="text/javascript">
    $("#formulario_agencia").validate({
        rules:{
            "nombre": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },
            "direccion": {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            "ciudad": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true
            },
            "pais": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true
            },
            "telefono": {
                required: true,
                minlength: 7,
                maxlength: 15,
                number: true
            },

          "longitud": {
              required: true
          },
          "latitud": {
              required: true
          }
        },
        messages:{
            "nombre": {
                required: "Por favor, ingrese el nombre",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "direccion": {
                required: "Por favor, ingrese la dirección",
                minlength: "La dirección debe tener al menos 5 caracteres",
                maxlength: "La dirección no puede tener más de 100 caracteres"
            },
            "ciudad": {
                required: "Por favor, ingrese la ciudad",
                lettersonly: "Solo letras",
                minlength: "La ciudad debe tener al menos 2 caracteres",
                maxlength: "La ciudad no puede tener más de 50 caracteres"
            },
            "pais": {
                required: "Por favor, ingrese el país",
                lettersonly: "Solo letras",
                minlength: "El país debe tener al menos 2 caracteres",
                maxlength: "El país no puede tener más de 50 caracteres"
            },
            "telefono": {
                required: "Por favor, ingrese el teléfono",
                number:"Solo numeros",
                minlength: "El teléfono debe tener al menos 7 caracteres",
                maxlength: "El teléfono no puede tener más de 15 caracteres"
            },
          
              "longitud": {
                required: "Por favor, ingrese la longitud"
              },
              "latitud": {
                required: "Por favor, ingrese la latitud"
              }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
