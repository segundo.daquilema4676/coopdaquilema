<div class="container">
  <h1>
    <b>
      <i class="fa fa-plus-circle"></i>
      NUEVO CAJERO
    </b>
  </h1>
  <br>
  <div class="row">
    <div class="col-md-6">
      <form action="<?php echo site_url('cajeros/guardarCajero');?>" method="post" enctype="multipart/form-data" id="formulario_cajero">

        <div class="form-group">
          <label for=""><b>AGENCIA:</b></label>
          <br>
          <select name="idAgencia" id="idAgencia" class="form-control" required>
              <?php foreach ($agencias as $agencia) : ?>
                  <option value="<?php echo $agencia->idAgencia; ?>" <?php echo ($agencia->idAgencia == $cajeroEditar->idAgencia) ? 'selected' : ''; ?>><?php echo $agencia->nombre; ?></option>
              <?php endforeach; ?>
          </select>
      </div>

        <div class="form-group">
          <label for="ubicacion">UBICACION:</label>
          <input type="text" name="ubicacion" id="ubicacion" class="form-control" placeholder="Ingrese la ubicacion">
        </div>
        <div class="form-group">
          <label for="estado">ESTADO:</label>
          <input type="text" name="estado" id="estado" class="form-control" placeholder="Ingrese el estado">
        </div>
        <div class="row">
          <div class="col-md-6">
            <label for="latitud">LATITUD:</label>
            <input type="number" name="latitud" id="latitud" class="form-control" placeholder="Ingrese la latitud" readonly>
          </div>
          <div class="col-md-6">
            <label for="longitud">LONGITUD:</label>
            <input type="number" name="longitud" id="longitud" class="form-control" placeholder="Ingrese la longitud" readonly>
          </div>
        </div>
        <b>Fotografia:</b>
      </label>
      <input type="file" name="foto"
      id="foto"
       class="form-control" required
      accept="image/*">

        <br>
        <div class="form-group">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
          <a href="<?php echo site_url('cajeros/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i> CANCELAR</a>
        </div>
      </form>
    </div>
    <div class="col-md-6">
      <div id="mapa" style="height: 350px; width: 100%; border: 1px solid black;"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-0.18522025692690522, -78.47587495002826);

    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Selecciona la ubicacion',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
<script type="text/javascript">
    $("#formulario_cajero").validate({
        rules:{
            "ubicacion": {
                required: true,
                minlength: 2,
                maxlength: 100,
                lettersonly: true

            },

            "estado": {
                required: true,
                minlength: 2,
                maxlength: 50,
                lettersonly: true

            },
            "foto": {
                required: true

          },
          "longitud": {
              required: true
          },
          "latitud": {
              required: true
          }
        },
        messages:{
            "ubicacion": {
                required: "Por favor, ingrese la ubicacion",
                lettersonly: "Solo letras",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "estado": {
                required: "Por favor, ingrese el estado",
                lettersonly: "Solo letras",
                minlength: "El país debe tener al menos 2 caracteres",
                maxlength: "El país no puede tener más de 50 caracteres"
            },

            "foto": {
                required: "Por favor, seleccione una fotografía"
              },
              "longitud": {
                required: "Por favor, ingrese la longitud"
              },
              "latitud": {
                required: "Por favor, ingrese la latitud"
              }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
