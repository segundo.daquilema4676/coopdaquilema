<?php
class Cliente extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }
  // Insertar nuevos clientes
  function insertar($datos){
    $respuesta=$this->db->insert("cliente",$datos);
    return $respuesta;
  }
  // Consultar todos los clientes
  function consultarTodos(){
    $clientes=$this->db->get("cliente");
    if ($clientes->num_rows()>0){
      return $clientes->result();
    } else {
      return false;
    }
  }

  // Eliminar cliente por id
  function eliminar($id){
    $this->db->where("idCliente",$id);
    return $this->db->delete("cliente");
  }
  // Consultar un único cliente por id
  function obtenerPorId($id){
    $this->db->where("idCliente",$id);
    $cliente=$this->db->get("cliente");
    if ($cliente->num_rows()>0) {
      return $cliente->row();
    } else {
      return false;
    }
  }


  // Función para actualizar clientes
  function actualizar($id,$datos){
    $this->db->where("idCliente",$id);
    return $this->db
                ->update("cliente",$datos);
  }

} // Fin de la clase
?>
