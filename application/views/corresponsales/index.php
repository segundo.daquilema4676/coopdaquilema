<h1>
  <i class="fa fa-store"></i>
  CORRESPONSALES
</h1>

<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver mapa
    </button>

    <a href="<?php echo site_url('corresponsales/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR CORRESPONSAL
    </a>
    <br><br>
  </div>
</div>
<?php if ($listadoCorresponsales): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>NOMBRE</th>
      <th>UBICACION</th>
      <th>TIPO</th>
      <th>HOARIO APERTURA</th>
      <th>HOARIO CIERRE</th>
      <th>LATITUD</th>
      <th>LONGITUD</th>
      <th>FOTO</th>
      <th>AGENCIA</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoCorresponsales as $corresponsal): ?>
    <tr>
      <td><?php echo $corresponsal->idCorresponsal; ?></td>
      <td><?php echo $corresponsal->nombre; ?></td>
      <td><?php echo $corresponsal->ubicacion; ?></td>
      <td><?php echo $corresponsal->tipo; ?></td>
      <td><?php echo $corresponsal->horario_apertura; ?></td>
      <td><?php echo $corresponsal->horario_cierre; ?></td>
      <td><?php echo $corresponsal->latitud; ?></td>
      <td><?php echo $corresponsal->longitud; ?></td>

          <td>
              <?php if ($corresponsal->foto!=""): ?>
                <img src="<?php echo base_url('uploads/corresponsales/').$corresponsal->foto; ?>" height="100px" alt="">
              <?php else: ?>
                N/A
              <?php endif; ?>
            </td>
            <td>
              <?php
              if ($corresponsal->idAgencia) {
                $agencia = $this->Agencia->obtenerPorId($corresponsal->idAgencia);
                echo $agencia->nombre;
              } else {
                echo 'N/A';
              }
              ?>
            </td>
             <td>
        <a href="<?php echo site_url('corresponsales/editar/') . $corresponsal->idCorresponsal; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>

        <a href="<?php echo site_url('corresponsales/borrar/') . $corresponsal->idCorresponsal; ?>">Eliminar</a>

      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          <i class="fa fa-eye"></i> MAPA DE AGENCIAS DE SUCURSAL
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </h5>
      </div>
      <div class="modal-body">
        <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">

          <script type="text/javascript">
            function initMap(){
              var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
              var miMapa = new google.maps.Map(
                document.getElementById('reporteMapa'),
                {
                  center: coordenadaCentral,
                  zoom: 8,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                }
              );
              <?php foreach ($listadoCorresponsales as $corresponsal): ?>
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $corresponsal->latitud; ?>, <?php echo $corresponsal->longitud; ?>);
                var icono = {
                    url: '<?php echo base_url('static/img/ico3.png'); ?>', // Ruta de la imagen que deseas utilizar como marcador
                    scaledSize: new google.maps.Size(50, 50), // Tamaño de la imagen
                    origin: new google.maps.Point(0, 0), // Origen de la imagen
                    anchor: new google.maps.Point(25, 50) // Punto de anclaje de la imagen
                };
                var marcador = new google.maps.Marker({
                  position: coordenadaTemporal,
                  map: miMapa,
                  title: 'Corresponsal:<?php echo $corresponsal->nombre; ?>',
                  icon: icono // Establece el icono personalizado

                });
              <?php endforeach; ?>
            }
          </script>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron agencias de sucursal registradas
</div>
<?php endif; ?>
